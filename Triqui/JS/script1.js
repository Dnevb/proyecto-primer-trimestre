turno = 0;
tablero = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0] //inicializamos vector(10 posiciones, posicion 0 -NO-la usamos. Se utiliza el vector apartir de posicion 1)

contador = document.getElementById('turno');
contador.innerHTML = turno;

function controlCell(num, nameId) {
  celda = document.getElementById(nameId); //Tomar el elemento en "celda"
  turno++;
  contador.innerHTML = turno; // contador que aparece en la pag

  if (!celda.disable) {

    if (turno % 2 == 0) {
      celda.style.backgroundImage = 'url("img/circulo.png")';
      celda.onclick = '';
      tablero[num] = 2;

      if (turno > 4) {
        prueba()
      }

    }

    if (turno % 2 == 1) {
      celda.style.backgroundImage = 'url("img/equis.png")';
      celda.onclick = '';
      tablero[num] = 1;

      if (turno > 4) {
        prueba()
      }

    }

  }
}
/**
 * ----------------IMPORTANTE---------------
 * Combinaciones para ganar!
 * Horizontales
 * celda1,celda2,celda3
 * celda4,celda5,celda6
 * celda7,celda8,celda9
 *
 * Verticales
 * celda1,celda4,celda7
 * celda2,celda5,celda8
 * celda3,celda6,celda9
 *
 * Diagonales
 * celda3,celda5,celda7
 * celda1,celda5,celda9
 * --------------------------------------------
 */


function prueba() {
  console.log(tablero);
  
  
  for (let i = 1; i < 8; i += 3) {
    console.log(i);
    

    if (tablero[i] == tablero[i+1] && tablero[i+1] == tablero[i+2]){
      
      if (tablero[i] == 1) {

        console.log("Gana 1");
        document.getElementById('ganador').innerHTML = ' GANA El JUGADOR 1! '
        document.getElementById('ganador').style.color = "blue"
        document.getElementById("botRes").style.display = "block"
        blockCells()

        break
      }

      if (tablero[i] == 2) {

        console.log("Gana 2");
        document.getElementById('ganador').innerHTML = ' GANA El JUGADOR 2! '
        document.getElementById('ganador').style.color = "red"
        document.getElementById("botRes").style.display = "block"
        blockCells()

        break
      }
    }
  }

  for (let i = 1; i < 3; i++) {

    if (tablero[i] == tablero[i+3] && tablero[i+3] == tablero[i+6]){
      
      if (tablero[i] == 1) {

        console.log("Gana 1");
        document.getElementById('ganador').innerHTML = ' GANA El JUGADOR 1! '
        document.getElementById('ganador').style.color = "blue"
        document.getElementById("botRes").style.display = "block"
        blockCells()

        break
      }

      if (tablero[i] == 2) {

        console.log("Gana 2");
        document.getElementById('ganador').innerHTML = ' GANA El JUGADOR 2! '
        document.getElementById('ganador').style.color = "red"
        document.getElementById("botRes").style.display = "block"
        blockCells()

        break
      }
    }
    
  }

  if (tablero[1] == tablero[5] && tablero[5] == tablero[9]) {
    
    if (tablero[1] == 1) {

      console.log("Gana 1")
      document.getElementById('ganador').innerHTML = ' GANA El JUGADOR 1! '
      document.getElementById('ganador').style.color = "blue"
      document.getElementById("botRes").style.display = "block"
      blockCells()
      
    }

    if (tablero[1] == 2) {

      console.log("Gana 2")
      document.getElementById('ganador').innerHTML = ' GANA El JUGADOR 2! '
      document.getElementById('ganador').style.color = "red"
      document.getElementById("botRes").style.display = "block"
      blockCells()
      
    }

  }

  if (tablero[3] == tablero[5] && tablero[5] == tablero[7]) {
    
    
    if (tablero[3] == 1) {

      console.log("Gana 1")
      document.getElementById('ganador').innerHTML = ' GANA El JUGADOR 1! '
      document.getElementById('ganador').style.color = "blue"
      document.getElementById("botRes").style.display = "block"
      blockCells()
      
    }

    if (tablero[3] == 2) {

      console.log("Gana 2")
      document.getElementById('ganador').innerHTML = ' GANA El JUGADOR 2! '
      document.getElementById('ganador').style.color = "red"
      document.getElementById("botRes").style.display = "block"
      blockCells()
      
    }

  }

}

function blockCells() {
  for (let i = 1; i <= 9; i++) {
    document.getElementById('celda' + i).onclick = '';

  }
}